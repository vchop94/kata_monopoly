const expect = require('chai').expect;
const Player = require('../src/Player');
const Game = require('../src/Game');

describe('Player tests', () =>{
    describe('Player Movement', () =>{
        it('Player on beginning location (numbered 0), rolls 7, ends up on location 7', () =>{

             const horse = new Player('horse');

             horse.move(7);

            expect(horse.position).to.equal(7);
        });
        it('Player on location numbered 39, rolls 6, ends up on location 5', ()=>{
            const horse = new Player('horse');

            horse.position = 39;
            horse.move(6);

            expect(horse.position).to.equal(5);
        });
    });
    describe('Landing on Go', () =>{
        it('During a turn a Player lands on Go and their balance increases by $200.', () =>{
            const horse = new Player('Horse');
            horse.position = 40;
            horse.salary = 0;
            horse.move(1);
            expect(horse.salary).to.equal(200);
        });
        it('During a turn a Player lands on some "normal" location and their balance does not change.', () =>{
            const horse = new Player('Horse');
            horse.position = 20;
            horse.salary = 0;
            horse.move(12);
            expect(horse.salary).to.equal(0);
        });
    });
    describe('Passing over Go', ()=>{
        it('Player starts before Go near the end of the Board, rolls enough to pass Go. The Player\'s balance increases by $200.', () =>{
        const horse = new Player('Horse');
        horse.position = 40;
        horse.salary = 0;
        horse.move(5);
        expect(horse.salary).to.equal(200);
        });
        it('Player starts on Go, takes a turn where the Player does not additionally land on or pass over Go. Their balance remains unchanged.', () =>{
            const horse = new Player('Horse');
            horse.position = 1;
            horse.salary = 0;
            horse.move(9);
            expect(horse.salary).to.equal(0);
        });
        it('Player passes go twice during a turn. Their balance increases by $200 each time for a total change of $400.', () =>{
            const horse = new Player('Horse');
            horse.position = 1;
            horse.salary = 0;
            horse.move(80);
            expect(horse.salary).to.equal(400);
        });
    });
    describe('Landing on Go To Jail', () =>{
        it('Player starts before Go To Jail, lands on Go To Jail, ends up on Just Visiting and their balance is unchanged.', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            horse.position = 30;

            const players = [horse, car];
            const monopoly = new Game(players);

            horse.move(1);
            monopoly.play(1);

            expect(horse.position).to.equal(11);
            expect(horse.salary).to.equal(1500);
                
        });
        it('Player starts before Go To Jail, rolls enough to pass over Go To Jail but not enough to land on or pass over go. Their balance is unchanged and they end up where the should based on what they rolled.', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            horse.position = 30;

            const players = [horse, car];
            const monopoly = new Game(players);

            horse.move(2);
            monopoly.play(1);

            expect(horse.position).to.equal(32);
            expect(horse.salary).to.equal(1500);
        });

    });
    describe('Landing on Income Tax', () =>{
        it('During a turn, a Player with an initial total worth of $1800 lands on Income Tax. The balance decreases by $180.', () =>{

            const horse = new Player('Horse');
            const car = new Player('Car');
            horse.position = 4;
            horse.salary = 1800;

            const players = [horse, car];
            const monopoly = new Game(players);

            horse.move(1);
            monopoly.play(1);

            expect(horse.salary).to.equal(1620);
        });
        it('During a turn, a Player with an initial total worth of $2200 lands on Income Tax. The balance decreases by $200.', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            horse.position = 4;
            horse.salary = 2200;

            const players = [horse, car];
            const monopoly = new Game(players);

            horse.move(1);
            monopoly.play(1);

            expect(horse.salary).to.equal(2000);
        });
        it('During a turn, a Player with an initial total worth of $0 lands on Income Tax. The balance decreases by $0.',() =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            horse.position = 4;
            horse.salary = 0;

            const players = [horse, car];
            const monopoly = new Game(players);

            horse.move(1);
            monopoly.play(1);

            expect(horse.salary).to.equal(0);
        });
        it('During a turn, a Player with an initial total worth of $2000 lands on Income Tax. The balance decreases by $200.', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            horse.position = 4;
            horse.salary = 2000;

            const players = [horse, car];
            const monopoly = new Game(players);

            horse.move(1);
            monopoly.play(1);

            expect(horse.salary).to.equal(1800);
        });
        it('During a turn, a Player passes over Income Tax. Nothing happens.', ()=>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            horse.position = 4;
            horse.salary = 2000;

            const players = [horse, car];
            const monopoly = new Game(players);

            horse.move(2);
            monopoly.play(1);

            expect(horse.salary).to.equal(2000);
        });
        it('Player takes a turn and lands on Luxury tax. Their balance decreases by $75.', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            horse.position = 38;
            horse.salary = 2000;

            const players = [horse, car];
            const monopoly = new Game(players);

            horse.move(1);
            monopoly.play(1);

            expect(horse.salary).to.equal(1925);
        });
        it('Player passes Luxury Tax during a turn. Their balance is unchanged.', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            horse.position = 38;
            horse.salary = 2000;

            const players = [horse, car];
            const monopoly = new Game(players);

            horse.move(2);
            monopoly.play(1);

            expect(horse.salary).to.equal(2000);
        });
    });
    describe('Release 2 optional, winner handler', () =>{
        it('If a player\'s balance ever goes below 0, they lose. If you\'re playing with 2 players, the other player is announced as the winner. Setup your players to demonstrate this happening.', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
           
            const players = [horse, car];
            const monopoly = new Game(players);

            horse.position = 5;
            monopoly.play(1)
            horse.salary = 0;
            monopoly.play(1);

            
            expect(monopoly.winner).to.equal(car);
            expect(monopoly.players.length).to.equal(1);

        });
        it('Allow for more than 2 players. Have one player run out of money and the other 2 continue playing through the remainder of the 20 rounds.', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            const hulk = new Player('Hulk');
            
            const players = [horse, car, hulk];
            const monopoly = new Game(players);

            horse.position = 5;
            monopoly.play(1)
            horse.salary = 0;
            monopoly.play(20);
            
            expect(monopoly.winner).to.be.null;
            expect(monopoly.players.length).to.equal(2);
        });
        it('Allow for more than 2 players. Have all but one player run out of money over time (not all during the same turn). Announce the one remaining player as a winner.', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            const hulk = new Player('Hulk');
            
            const players = [horse, car, hulk];
            const monopoly = new Game(players);

            horse.position = 5;
            monopoly.play(1)
            horse.salary = 0;
            monopoly.play(1);
            car.salary = 0;
            monopoly.play(1);

            expect(monopoly.winner).to.equal(hulk);
            expect(monopoly.players.length).to.equal(1);
        });
    });
});