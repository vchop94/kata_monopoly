const expect = require('chai').expect;
const Game = require('../src/Game');
const Player = require('../src/Player');

describe('Game tests', () =>{
    describe('Game Players Tests', () =>{
        it('Create a game with two players named Horse and Car.', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');

            const players = [horse, car];

            const monopoly = new Game(players);

            expect(monopoly).to.not.be.null;
        });
        it('Try to create a game with < 2 players. When attempting to play the game, it will fail.', () =>{
            const horse = new Player('Horse');
            const players = [horse];
           
            expect(function(){
                 new Game(players);
            }).to.throw(Error, 'Game not created');
           
        });
        it('Try to create a game with > 8 players. When attempting to play the game, it will fail.', () =>{
            const moreThenEightPlayers = [];

            for(let i = 0; i <10; i++){
                moreThenEightPlayers.push(new Player('Horse' + i));
            }

            expect(function(){
            
              new Game(moreThenEightPlayers);

            }).to.throw(Error, 'Game not created');
        });
        it('Create a game with two players named Horse and Car. Within creating 100 games, both orders [Horse, Car] and [car, horse] occur.', ()=>{
            const horse = new Player('Horse');
            const car = new Player('Car');

            const players = [horse, car];

            let orderHorseCar = false;
            let orderCarHorse = false;

        
            for(let i = 0; i < 10; i++){
                const monopoly = new Game(players);
                    
                if(monopoly.players[0].nome === 'Horse'){
                    orderHorseCar = true;
                }else if(monopoly.players[0].nome === 'Car'){
                    orderCarHorse = true;
                }
            }
        
        expect(orderHorseCar).to.be.true;
        expect(orderCarHorse).to.be.true;
                

        });
    });
    describe('Game Rounds tests', () =>{
        it('Create a game and play, verify that the total rounds was 20 and that each player played 20 rounds.', () =>{
            const horse = new Player('horse');
            const car = new Player('Car');
            const players = [horse, car];

            const monopoly = new Game(players);

            monopoly.play(20);

            expect(monopoly.rounds).to.equal(20);
            expect(horse.roundsPlayed).to.equal(20);
            expect(car.roundsPlayed).to.equal(20);
        });
        it('Create a game and play, verify that in every round the order of the players remained the same.', () =>{
            const horse = new Player('horse');
            const car = new Player('Car');
            const players = [horse, car];

            const monopoly = new Game(players);

            const monopolyPlayers = monopoly.players;

            monopoly.play(20);

            expect(monopoly.players).to.equal(monopolyPlayers);
        });
    });
    describe('Real Estate, buy property', () =>{
        it('Land on a Property that is not owned. After turn, property is owned and balance decreases by cost of property.', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            const players = [horse, car];

            const monopoly = new Game(players);

            horse.move(2);

            horse.buy(monopoly.properties[horse.position - 1]);
            monopoly.play(1);

            //player initial salary is allways 1500
            expect(horse.salary).to.equal(1500 - monopoly.properties[horse.position - 1].price);

            
        });
        it('Land on a Property that I own, nothing happens.', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            const players = [horse, car];

            const monopoly = new Game(players);

            horse.move(2);

            horse.buy(monopoly.properties[horse.position - 1]);
            monopoly.play(1);

            horse.move(4);
            monopoly.play(1);

            horse.position = 2;
            monopoly.play(1);

            //player initial salary is allways 1500, il test è lo stesso per la similitudine delle richieste.
            //il programma in automatico non sottrae niente se non viene chiamato buy o payrent...
            expect(horse.salary).to.equal(1500 - monopoly.properties[horse.position - 1].price);
        });
        it('Pass over an unowned Property, nothing happens.', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            const players = [horse, car];

            const monopoly = new Game(players);

            horse.move(4);
            monopoly.play(1);

            expect(horse.salary).to.equal(1500);
         

        });
    });
    describe('Real State, pay rent', () =>{
        it('If landing on Railroad, rent is 25, 50, 100, 200 depending on how many are owned by owner (1 of 4).', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            const players = [horse, car];

            const monopoly = new Game(players);

            horse.move(6);
            horse.buy(monopoly.properties[horse.position -1]);
            monopoly.play(1);

            car.move(6);
            monopoly.play(1);

            expect(car.salary).to.equal(1475);
            
        });
        it('If landing on Railroad, rent is 25, 50, 100, 200 depending on how many are owned by owner (2 of 4).', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            const players = [horse, car];

            const monopoly = new Game(players);

            horse.move(6);
            horse.buy(monopoly.properties[horse.position -1]);
            monopoly.play(1);
            horse.move(10);
            horse.buy(monopoly.properties[horse.position -1]);
            monopoly.play(1);

            car.move(6);
            monopoly.play(1);

            expect(car.salary).to.equal(1450);
        });
        it('If landing on Railroad, rent is 25, 50, 100, 200 depending on how many are owned by owner (3 of 4).', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            const players = [horse, car];

            const monopoly = new Game(players);

            horse.move(6);
            horse.buy(monopoly.properties[horse.position -1]);
            monopoly.play(1);
            horse.move(10);
            horse.buy(monopoly.properties[horse.position -1]);
            monopoly.play(1);
            horse.move(10);
            horse.buy(monopoly.properties[horse.position -1]);
            monopoly.play(1);

            car.move(6);
            monopoly.play(1);

            expect(car.salary).to.equal(1400);
        });
        it('If landing on Railroad, rent is 25, 50, 100, 200 depending on how many are owned by owner (4 of 4).', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            const players = [horse, car];

            const monopoly = new Game(players);

            horse.move(6);
            horse.buy(monopoly.properties[horse.position -1]);
            monopoly.play(1);
            horse.move(10);
            horse.buy(monopoly.properties[horse.position -1]);
            monopoly.play(1);
            horse.move(10);
            horse.buy(monopoly.properties[horse.position -1]);
            monopoly.play(1);
            horse.move(10);
            horse.buy(monopoly.properties[horse.position -1]);
            monopoly.play(1);

            car.move(6);
            monopoly.play(1);

            expect(car.salary).to.equal(1300);
        });
        it('If landing on Utility and only one Utility owned, rent is 4 times current value on Dice.', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            const players = [horse, car];

            const monopoly = new Game(players);

            horse.move(13);
            horse.buy(monopoly.properties[horse.position -1]);
            monopoly.play(1);

            const diceVal = monopoly.rollDice();

            car.position = 13;
            monopoly.play(1, diceVal);
  
            expect(car.salary).to.equal(1500 - (diceVal * 4));

            
        });
        it('If landing on Utility and both owned (not necessarily by same Player), rent is 10 times current value on Dice.', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            const hulk = new Player('Hulk');
            const players = [horse, car, hulk];

            const monopoly = new Game(players);

            horse.move(13);
            horse.buy(monopoly.properties[horse.position -1]);
            monopoly.play(1);
            hulk.move(29);
            hulk.buy(monopoly.properties[hulk.position -1]);
            monopoly.play(1);

            const diceVal = monopoly.rollDice();

            car.position = 13;
            monopoly.play(1, diceVal);
  
            expect(car.salary).to.equal(1500 - (diceVal * 10));

        });
        it('If landing on Real Estate and not all in the same Property Group are owned, rent is stated rent value.', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            const players = [horse, car];

            const monopoly = new Game(players);

            horse.move(2);
            horse.buy(monopoly.properties[horse.position -1]);
            monopoly.play(1);

            car.move(2);
            monopoly.play(1);

            expect(car.salary).to.equal(1498);
        });
        it('If landing on Real Estate and Owner owns all in the same Property Group, rent is 2 times stated rent value.', () =>{
            const horse = new Player('Horse');
            const car = new Player('Car');
            const players = [horse, car];

            const monopoly = new Game(players);

            horse.move(2);
            horse.buy(monopoly.properties[horse.position -1]);
            monopoly.play(1);

            horse.move(2);
            horse.buy(monopoly.properties[horse.position -1]);
            monopoly.play(1);

            car.move(4);
            monopoly.play(1);
            console.log(horse.properties[1].rent);
            expect(car.salary).to.equal(1492);
        });
    });
});