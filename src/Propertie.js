'use strict';

class Propertie {
    constructor(nome, id, price, rent, group, classe){
        this.nome = nome;
        this.price = price;
        this.id = id;
        this.rent = rent;
        this.owner = null;
        this.group = group;
        this.classe = classe;
       
    }

    setOwner(newOwner){
        this.owner = newOwner;
    }

    payRent(player, game, diceVal){
        switch(this.classe){
            
            case 'railroads':
                let contR = 0;
                let owner = this.owner;

                for(let i = 0; i < owner.properties.length; i++){
                    if(owner.properties[i].group === 'railroads'){
                        contR++;
                    }
                }
            
                if(contR === 1){
                    player.salary -= 25;
                    this.owner.salary += 25;
                }else if(contR === 2){
                    player.salary -= 50;
                    this.owner.salary += 25;
                }else if(contR === 3){
                    player.salary -= 100;
                    this.owner.salary += 25;
                }else if(contR === 4){
                    player.salary -= 200;
                    this.owner.salary += 25;
                }
            break;
            case 'utilities':
                let contU = 0;
                if(game.properties[12].owner !== null){ contU++ }
                if(game.properties[28].owner !== null) { contU++ }

                if(contU === 1){
                    player.salary -= diceVal * 4;
                    this.owner.salary += diceVal * 4;
                }else if(contU === 2){
                    player.salary -= diceVal * 10;
                    this.owner.salary += diceVal * 10;
                }
            break;
            case 'property': 
                let contS = 0;
              
                for(let i = 0; i < game.properties.length; i++){
                    if(game.properties[i].owner === this.owner) {contS++}
                }

                
                if(this.group === 'brown'
                    || this.group === 'blue'){
                    if(contS === 2){
                        this.rent *= 2;
                        player.salary -= this.rent;
                        this.owner += this.rent;
                    }else{
                        player.salary -= this.rent;
                        this.owner += this.rent;
                    }
                }else if(this.group === 'lightblue'
                    || this.group === 'violet'
                    || this.group === 'orange'
                    || this.group === 'red'
                    || this.group === 'yellow'
                    || this.group === 'green'){
                        if(this.contS === 3){
                            this.rent *= 2;
                            player.salary -= this.rent;
                            this.owner += this.rent;
                        }else{
                            player.salary -= this.rent;
                            this.owner += this.rent;
                        }
                }
                break;
        }

    }
}

module.exports = Propertie;