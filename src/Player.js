class Player {
    constructor(nome){
        this.nome = nome;
        this.salary = 1500;
        this.position = 0;
        this.properties = [];
        this.roundsPlayed = 0;
    }

    move(diceVal){

        this.position += diceVal;

        while(this.position > 40){
            this.position -= 40;
            this.salary += 200;
        }
    }

    buy(propertie){
        this.salary -= propertie.price;
        this.properties.push(propertie);
        propertie.setOwner(this);
    }

    playTurn(){
        this.roundsPlayed++;
    }
}


module.exports = Player;

