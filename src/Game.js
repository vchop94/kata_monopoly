'use strict';

const propertie = require('./Propertie.js');

class Game{
    constructor(players){
        if(players.length < 2 || players.length > 8){
            throw new Error('Game not created');
        }else{
            this.rounds = 0;
            this.players = this.shufflePlayers(players);
            this.winner = null;
            this.finished = false;
            this.lastDiceVal = 0;
            this.properties = [];
            this.setProperties();
        }
    }

    setProperties(){
        this.properties[0] = new propertie('Go', 1, 0, 0, 'cornercell', "corner");
        this.properties[1] = new propertie('Mediterranean Avenue', 2, 60, 2, 'brown', 'property');
        this.properties[2] = new propertie('Community Chest', 3, 0, 0 , 'community chest', "chest");
        this.properties[3] = new propertie('Baltic Avenue', 4, 60, 4, 'brown', 'property');
        this.properties[4] = new propertie('Income Tax', 5, 0, 0, 'tax', 'tax');
        this.properties[5] = new propertie('Reading Railroad', 6, 200, 0, 'railroads', 'railroads');
        this.properties[6] = new propertie('Oriental Avenue', 7, 100, 6, 'lightblue', 'property');
        this.properties[7] = new propertie('Chance', 8, 0, 0, 'chance', 'chance');
        this.properties[8] = new propertie('Vermont Avenue', 9, 100, 6, 'lightblue', 'property');
        this.properties[9] = new propertie('Connecticut Ave', 10, 120, 8, 'lightblue', 'property');
        this.properties[10] = new propertie('In jail just visiting', 11, 0, 0, 'cornercell', 'corner');
        this.properties[11] = new propertie('St. Charles Place', 12, 140, 10, 'violet', 'property');
        this.properties[12] = new propertie('Eletric Company', 13, 150, 0, 'utilities', 'utilities');
        this.properties[13] = new propertie('States Avenue', 14, 140, 10, 'violet', 'property');
        this.properties[14] = new propertie('Virginia Avenue', 15, 160, 12, 'violet', 'property');
        this.properties[15] = new propertie('Pennsylvania Railroad', 16, 200, 0 , 'railroads', 'railroads');
        this.properties[16] = new propertie('St. James Place', 17, 180, 14, 'orange', 'property');
        this.properties[17] = new propertie('Community Chest', 18, 0, 0 , 'community chest', 'community chest');
        this.properties[18] = new propertie('Tennessee Avenue', 19, 180, 14, 'orange', 'property');
        this.properties[19] = new propertie('New York Avenue', 20, 200, 16, 'orange', 'property');
        this.properties[20] = new propertie('Free Parking', 21, 0, 0, 'cornercells', 'corner');
        this.properties[21] = new propertie('Kentycky Avenue', 22, 220, 18, 'red', 'property');
        this.properties[22] = new propertie('Chance', 23, 0, 0, 'chance', 'chance');
        this.properties[23] = new propertie('Indiana Avenue', 24, 220, 18, 'red', 'property');
        this.properties[24] = new propertie('Illinois Avenue', 25, 240, 20, 'red', 'property');
        this.properties[25] = new propertie('B&O Railroad', 26, 200, 0, 'railroads', 'railroads');
        this.properties[26] = new propertie('Atlantic Avenue', 27, 260, 22, 'yellow', 'property');
        this.properties[27] = new propertie('Ventnor Avenue', 28, 260, 22, 'yellow', 'property');
        this.properties[28] = new propertie('Water Works', 29, 150, 0, 'utilities', 'utilities');
        this.properties[29] = new propertie('Marvin Gardens', 30, 280, 24, 'yellow', 'property');
        this.properties[30] = new propertie('Go to jail', 31, 0, 0, 'cornercells', 'corner');
        this.properties[31] = new propertie('Pacific Avenue', 32, 300, 26, 'green', 'property');
        this.properties[32] = new propertie('North Carolina Avenue', 33, 300, 26, 'green', 'property');
        this.properties[33] = new propertie('Community Chest', 34, 0, 0 , 'community chest', 'community chest');
        this.properties[34] = new propertie('Pennsylvania Avenue', 35, 320, 28, 'green', 'property');
        this.properties[35] = new propertie('Short Line Railroad', 36, 200, 0, 'railroads', 'railroads');
        this.properties[36] = new propertie('Chance', 37, 0, 0, 'chance', 'chance');
        this.properties[37] = new propertie('Park Place', 38, 350, 35, 'blue', 'property');
        this.properties[38] = new propertie('Luxury Tax', 39, 0, 0, 'tax', 'tax');
        this.properties[39] = new propertie('BoardWalk', 40, 400, 50, 'blue', 'property');
    }
    shufflePlayers(players){
        
        for (let i = players.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            let temp = players[i];
            players[i] = players[j];
            players[j] = temp;
        }

        return players;
    }


    positionHandler(playerOfTheTurn, diceVal){
        if(playerOfTheTurn.position === 5){
            if(playerOfTheTurn.salary <= 2000){
                playerOfTheTurn.salary -= playerOfTheTurn.salary * 10 / 100;
            }
            else{
                playerOfTheTurn.salary -= 200;
            }
        }
        else if(playerOfTheTurn.position === 31){
                    playerOfTheTurn.position = 11;
        }
        else if(playerOfTheTurn.position === 39){
            playerOfTheTurn.salary -= 75;
        }else if(playerOfTheTurn.position > 0){
            if(this.properties[playerOfTheTurn.position - 1].owner !== null
                && this.properties[playerOfTheTurn.position- 1].owner !== playerOfTheTurn){
                    this.properties[playerOfTheTurn.position -1].payRent(playerOfTheTurn, this, this.lastDiceVal);
                }
        }     
    }

    winnerHandler(){
       for(let i = 0; i < this.players.length; i++){
            if(this.players[i].salary === 0){
                this.players.splice(i, 1);
            }
        }

        if(this.players.length === 1){
            this.winner = this.players[0];
            this.finished = true;
        }
    }
    
    rollDice(){
        let result = Math.floor( Math.random() * 12 ) + 1;
        this.lastDiceVal = result;
        return result;
    }

    play(totalRounds, diceVal){
        for(let i = 1; i <= totalRounds; i++){
            this.rounds++;
            for(let j = 0; j < this.players.length; j++){
                let playerOfTheTurn = this.players[j];

                playerOfTheTurn.playTurn();
                this.positionHandler(playerOfTheTurn, diceVal);
                this.winnerHandler();
                    
            }
        }
    }
}

module.exports = Game;